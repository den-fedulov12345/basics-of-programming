package loops.advanced;

import java.util.Scanner;

public class AdvLoops002 {

    public static void main(String[] args) {

        // create scanner object
        Scanner in = new Scanner(System.in);

        // here, we just declare that the correct name is "Emma"
        String correctName = "Emma";

        // this number will keep track of the total number of guesses
        int guesses = 0;

        // we use a while (true) loop which will loop forever because the condition for the loop will always be
        // literally "true"
        while (true) {
            // we increment the number of guesses each time the loop passes
            guesses++;

            // let the user guess the name
            System.out.println("Guess my name (type stop to exit)");
            String guessed = in.nextLine();

            // !!IMPORTANT - we can't use "guessed == correctName". The reason is kinda complicated but if you want,
            // just call me, and I'll explain lol
            if (guessed.equals(correctName)) {
                System.out.println("Congratulations!");

                // 'break' is used to break out of a loop (in our case the infinity loop)
                break;
            }
            if (guessed.equals("stop")) {
                // if the user types "stop", we want to decrease the guess count because the "guesses" count has
                // increased even though the user didn't guess... He just wrote "stop"
                guesses--;
                break;
            }
        }

        // lastly, print out the number of guesses
        System.out.println("You guessed " + guesses + " times.");

    }

}
