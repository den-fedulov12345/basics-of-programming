package methods.syntax;

public class Syntax003 {

    public void sumTheValues(int number1, int number2) {

        System.out.println("The sum is " + (number1 + number2) + ".");

    }

}
