package methods.syntax;

public class Syntax002 {

    // apparently this method needs to be "static"
    public static void printHelloHamk() {
        System.out.println("Hello HAMK");
    }

}
