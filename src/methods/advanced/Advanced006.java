package methods.advanced;

public class Advanced006 {

    public void calculate(int number1, int number2, String operation) {

        int result = 0;

        // I added a neat switch because it seemed like cleaner code. If you need help on how to use switches,
        // refer to https://www.w3schools.com/java/java_switch.asp
        switch (operation) {

            // the tester has a typo here. It should be "subtraction" and not "subStraction"
            case "substraction":
                result = number1 - number2;
                break;
            case "sum":
                result = number1 + number2;
                break;
            case "multiplication":
                result = number1 * number2;
                break;
        }

        System.out.println("The result is " + result + ".");

    }

}
