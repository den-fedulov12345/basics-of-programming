package methods.advanced;

public class Advanced003 {

    public String aphorismPrinter(int index) {

        // we first refine the aphorisms in an array
        String[] aphorisms = {
                "Actions speak louder than words.",
                "A barking dog never bites.",
                "A penny saved is a penny earned.",
                "All things come to those who wait."
        };

        // then, we print the aphorism at the index of [index] that the user has input
        return aphorisms[index];
    }

}
