package methods.advanced;

public class Advanced004 {

    public void formatSentence(String name, int height) {

        System.out.println(name + " is " + height + " cm tall.");

    }

}
