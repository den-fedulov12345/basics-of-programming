package methods.basics;

public class Basics005 {

    public void printBigger(int number1, int number2) {
        if (number1 > number2)
            System.out.println(number1);
        else if (number2 > number1)
            System.out.println(number2);
        else
            System.out.println("The numbers are equal.");
    }

}
