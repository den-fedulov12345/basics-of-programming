package arrays.basics;

public class Arrays005 {

    public static void main(String[] args) {

        // here, we create our array and initialize it with the correct values
        int[] array = {3, 6, 1};

        // before the for loop, we create a "sum" variable. Then, when we loop through the list, we just add every value
        // to this special "sum" variable. Then, we print it.
        int sum = 0;

        for (int i : array) {
            sum = sum + i;
        }

        System.out.println(sum);

    }

}
