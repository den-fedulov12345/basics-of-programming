package arrays.basics;

public class Arrays004 {

    public static void main(String[] args) {

        // furniture array
        String[] furniture = {"Table", "Sofa", "Shelf", "Painting"};

        // "for every String thing in furniture, check if it is a Sofa. Once the Sofa is found, print Sofa found and
        // exit the array"
        for (String thing : furniture) {

            // There is a reason why I'm using thing.equals() instead of the regular "==" comparison but the reason
            // is a bit complicated. For now, you can use either of them, but it's better to use .equals().
            if (thing.equals("Sofa")) {
                System.out.println("Sofa found");

                // this is a special statement used when we want to "break out" of a loop. In this case, it breaks out
                // of the "for" loop and thus ends the program.
                break;
            }
        }

    }

}
